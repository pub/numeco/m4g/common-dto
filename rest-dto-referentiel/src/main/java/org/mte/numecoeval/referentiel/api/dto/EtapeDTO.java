package org.mte.numecoeval.referentiel.api.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;
import lombok.experimental.FieldDefaults;

import java.io.Serializable;

@Getter
@Setter
@AllArgsConstructor
@EqualsAndHashCode
@Accessors(chain = true)
@FieldDefaults(level = AccessLevel.PRIVATE)
@Builder
@Schema(
        description = "Référentiel d'étape dans le cycle de vie d'un équipement (Etape ACV)"
)
public class EtapeDTO implements Serializable {
    @Schema(
            description = "Code de l'étape. Ne contient que des majuscules, clé du référentiel",
            pattern = "[A-Z]+"
    )
    String code;
    @Schema(
            description = "Libellé de l'étape"
    )
    String libelle;
}
