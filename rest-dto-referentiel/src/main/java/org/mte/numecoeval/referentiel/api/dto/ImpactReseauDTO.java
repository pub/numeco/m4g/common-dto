package org.mte.numecoeval.referentiel.api.dto;


import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.FieldDefaults;

import java.io.Serializable;

@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
@Builder
@FieldDefaults(level = AccessLevel.PRIVATE)
@EqualsAndHashCode
@Schema(
        description = "Référentiel de l'impact écologique d'un équipement physique vis à vis de l'usage du réseau dans les référentiels. La clé est composé des champs refReseau, etapeACV et critere."
)
public class ImpactReseauDTO implements Serializable {
    @Schema(
            description = "Référence de l'usage du réseau, fait partie de la clé du référentiel"
    )
    @JsonProperty("refReseau")
    String refReseau;
    @Schema(
            description = "Étape ACV concerné pour l'usage du réseau, fait partie de la clé du référentiel"
    )
    @JsonProperty("etapeACV")
    String etapeACV;
    @Schema(
            description = "Critère d'impact écologique concerné pour l'usage du réseau, fait partie de la clé du référentiel"
    )
    @JsonProperty("critere")
    String critere;
    @Schema(
            description = "Unité de l'impact écologique concerné pour l'usage du réseau. Champ Déprécié",
            deprecated = true
    )
    @JsonProperty("unite")
    String unite;
    @Schema(
            description = "Source de l'impact écologique"
    )
    @JsonProperty("source")
    String source;
    @Schema(
            description = "Valeur de l'impact écologique"
    )
    @JsonProperty("valeur")
    Double valeur;
    @Schema(
            description = "Consommation électrique moyenne de l'impact écologique"
    )
    @JsonProperty("consoElecMoyenne")
    Double consoElecMoyenne;
}