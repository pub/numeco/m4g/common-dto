package org.mte.numecoeval.referentiel.api.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.FieldDefaults;

import java.io.Serializable;

@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
@Builder
@FieldDefaults(level = AccessLevel.PRIVATE)
@EqualsAndHashCode
@Schema(
        description = "Référentiel des mix électrique couvrant l'usage de l'électricité vis à vis du pays d'utilisation de l'équipement. La clé du référentiel est composé des champs pays et critere."
)
public class MixElectriqueDTO implements Serializable {

    @Schema(
            description = "Pays concerné, fait partie de la clé du référentiel"
    )
    String pays;
    @Schema(
            description = "Code du pays concerné en anglais"
    )
    String raccourcisAnglais;
    @Schema(
            description = "Critère d'impact écologique concerné, fait partie de la clé du référentiel"
    )
    String critere;
    @Schema(
            description = "Valeur du référentiel"
    )
    Double valeur;
    @Schema(
            description = "Source de la valeur du référentiel"
    )
    String source;
}
