package org.mte.numecoeval.referentiel.api.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;
import lombok.experimental.FieldDefaults;

import java.io.Serializable;

@Getter
@Setter
@AllArgsConstructor
@Accessors(chain = true)
@FieldDefaults(level = AccessLevel.PRIVATE)
@EqualsAndHashCode
@Builder
@Schema(
        description = "Référentiel des hypothèses utilisées pour les calculs"
)
public class HypotheseDTO implements Serializable {
    @Schema(
            description = "Code de l'hypothèse, clé du référentiel"
    )
    String code;
    @Schema(
            description = "Valeur de l'hypothèse"
    )
    String valeur;
    @Schema(
            description = "Source de l'hypothèse"
    )
    String source;
}
