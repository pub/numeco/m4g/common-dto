package org.mte.numecoeval.referentiel.api.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;
import lombok.experimental.FieldDefaults;

import java.io.Serializable;

@Getter
@Setter
@AllArgsConstructor
@Accessors(chain = true)
@FieldDefaults(level = AccessLevel.PRIVATE)
@EqualsAndHashCode
@Builder
@Schema(
        description = "Référentiel de critère d'impact écologique"
)
public class CritereDTO implements Serializable {
    @Schema(
            description = "Nom du critère d'impact écologique, clé du référentiel"
    )
    String nomCritere;
    @Schema(
            description = "Unité du critère d'impact écologique"
    )
    String unite;
    @Schema(
            description = "Description du critère d'impact écologique"
    )
    String description;
}
