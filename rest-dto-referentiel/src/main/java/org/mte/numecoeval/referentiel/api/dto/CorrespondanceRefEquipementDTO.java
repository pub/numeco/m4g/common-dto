package org.mte.numecoeval.referentiel.api.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.Accessors;
import lombok.experimental.FieldDefaults;

import java.io.Serializable;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
@FieldDefaults(level = AccessLevel.PRIVATE)
@EqualsAndHashCode
@Builder
@Schema(
        description = "Référentiel de correspondance entre un modèle d'équipement physique et une référence d'équipement dans les référentiels."
)
public class CorrespondanceRefEquipementDTO implements Serializable {
   @Schema(
           description = "Modèle de l'équipement, clé du référentiel"
   )
   String modeleEquipementSource;
   @Schema(
           description = "Référence d'équipement correspondant au modèle de l'équipement"
   )
   String refEquipementCible;
}
