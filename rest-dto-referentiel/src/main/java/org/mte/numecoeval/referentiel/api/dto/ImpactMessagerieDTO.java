package org.mte.numecoeval.referentiel.api.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.Accessors;
import lombok.experimental.FieldDefaults;

import java.io.Serializable;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
@FieldDefaults(level = AccessLevel.PRIVATE)
@EqualsAndHashCode
@Builder
@Schema(
        description = "Référentiel de l'impact écologique d'une messagerie. La clé est le champ critere. Chaque entrée représente les composants d'une fonction affine (Ax+b)."
)
public class ImpactMessagerieDTO implements Serializable {
    @Schema(
            description = "Coefficient directeur de la fonction affine"
    )
    Double constanteCoefficientDirecteur;
    @Schema(
            description = "Constante de la fonction affine"
    )
    Double constanteOrdonneeOrigine;
    @Schema(
            description = "Critère de l'impact écologique d'une messagerie, clé du référentiel"
    )
    String critere;
    @Schema(
            description = "Source de l'impact écologique d'une messagerie"
    )
    String source;
}
