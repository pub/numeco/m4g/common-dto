package org.mte.numecoeval.referentiel.api.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.FieldDefaults;
import org.springframework.http.HttpStatus;

import java.io.Serializable;
import java.time.LocalDateTime;


@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
@Builder
@FieldDefaults(level = AccessLevel.PRIVATE)
@EqualsAndHashCode
@Schema(
        description = "Objet standard pour les réponses en cas d'erreur d'API"
)
public class ErrorResponseDTO implements Serializable {
    @Schema(
            description = "Code de l'erreur"
    )
    int code;
    @Schema(
            description = "Message de l'erreur"
    )
    String message;
    @Schema(
            description = "Statut HTTP de la réponse"
    )
    HttpStatus status;
    @Schema(
            description = "Date & Heure de l'erreur"
    )
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy HH:mm:ss")
    LocalDateTime timestamp;

}
