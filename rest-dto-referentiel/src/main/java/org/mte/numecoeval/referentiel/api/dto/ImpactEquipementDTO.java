package org.mte.numecoeval.referentiel.api.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.FieldDefaults;

import java.io.Serializable;

@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
@Builder
@FieldDefaults(level = AccessLevel.PRIVATE)
@EqualsAndHashCode
@Schema(
        description = "Référentiel de l'impact écologique d'un équipement physique dans les référentiels. La clé est composé des champs refEquipement, etape et critere."
)
public class ImpactEquipementDTO implements Serializable {
    @Schema(
            description = "Référence de l'équipement physique, fait partie de la clé dans le référentiel"
    )
    String refEquipement;
    @Schema(
            description = "Étape ACV concernée, fait partie de la clé dans le référentiel"
    )
    String etape;
    @Schema(
            description = "Critère d'impact écologique concerné, fait partie de la clé dans le référentiel"
    )
    String critere;
    @Schema(
            description = "Source de l'impact écologique pour cette équipement physique"
    )
    String source;
    @Schema(
            description = "Type de l'équipement physique concerné"
    )
    String type;
    @Schema(
            description = "Valeur de l'impact écologique"
    )
    Double valeur;
    @Schema(
            description = "Consommation électrique moyenne"
    )
    Double consoElecMoyenne;
    @Schema(
            description = "Description de l'entrée dans le référentiel"
    )
    String description;
}
